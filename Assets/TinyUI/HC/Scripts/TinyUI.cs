﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Animator))]
public class TinyUI : MonoBehaviour
{
    Animator Animator;
    protected virtual void Awake()
    {
        Animator = GetComponent<Animator>();
    }
    protected virtual void OnEnable()
    {
        StartCoroutine(AnimationController("Open"));
    }
    protected virtual void OnDisable()
    {
        
    }
    internal virtual void CloseScreen()
    {
        StartCoroutine(AnimationController("Close", () => { this.gameObject.SetActive(false); }));
    }

    WaitForEndOfFrame wfeof = new WaitForEndOfFrame();
    protected IEnumerator AnimationController(string anim, System.Action func = null)
    {
        Animator.Play(anim, 0, 0);
        yield return wfeof;
        yield return new WaitForSeconds(Animator.GetCurrentAnimatorStateInfo(0).length);
        if (func != null)
            func();
    }
}
