﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum TinyUIType
{
    None,
    Start,
    Game,
    Result
}
public class TinyUIController : MonoBehaviour
{
    public static TinyUIController Instance { get; private set; }
    private void Awake()
    {
        if (Instance == null)
            Instance = this;
    }
    private void Start()
    {
        OpenStartScreen();
    }
    [HideInInspector]
    public TinyUIType PreviousType = TinyUIType.None;
    [HideInInspector]
    public TinyUIType CurrentUIType = TinyUIType.None;

    public TinyStartScreen StartScreen;
    public TinyGameScreen GameScreen;
    public TinyResultScreen ResultScreen;

    TinyUI previousScreen;
    TinyUI currentScreen;
    void OpenCanvas(TinyUI screenObject, TinyUIType currentType)
    {
        if (currentScreen != null)
        {
            CloseScreen(currentScreen);
            previousScreen = currentScreen;
            PreviousType = CurrentUIType;
        }
        if (screenObject != null)
        {
            screenObject.gameObject.SetActive(true);
            currentScreen = screenObject;
            CurrentUIType = currentType;
        }
    }

    void CloseScreen(TinyUI tinyUI)
    {
        tinyUI.CloseScreen();
    }

    public void OpenStartScreen()
    {
        OpenCanvas(StartScreen, TinyUIType.Start);
    }
    public void OpenGameScreen()
    {
        OpenCanvas(GameScreen, TinyUIType.Game);
    }
    public void OpenResultScreen()
    {
        OpenCanvas(ResultScreen, TinyUIType.Result);
    }
}
